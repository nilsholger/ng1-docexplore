'use strict';

var INTEGER_REGEXP = /^-?\d+$/;

angular.module('myApp.view15', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view15', {
    templateUrl: 'view15/view15.html',
    controller: 'View15Ctrl'
  });
}])

.controller('View15Ctrl', ['$scope', function($scope) {

  $scope.title = 'About Us';

}]);
