'use strict';

angular.module('myApp.view5', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view5', {
    templateUrl: 'view5/view5.html',
    controller: 'View5Ctrl'
  });
}])

.controller('View5Ctrl', ['$scope', function($scope) {
        $scope.title = 'More Directives';
}])
.directive('myDraggable', ['$document', function($document){
        return {
            link: function(scope, element, attr){
              var startX = 0, startY = 0, x = 0, y = 0;

              element.css({
                position: 'relative',
                border: '1px solid red',
                backgroundColor: 'lightgrey',
                cursor: 'pointer'
              });

              element.on('mousedown', function(event){
                event.preventDefault();
                startX = event.pageX - x;
                startY = event.pageY - y;
                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
              });

              function mousemove(event){
                y = event.pageY - startY;
                x = event.pageX - startX;
                element.css({
                  top: y + 'px',
                  left: x + 'px'
                });
              }
              function mouseup(){
                  $document.off('mousemove', mousemove);
                  $document.off('mouseup', mouseup);
              }
            }
        };
}])
.directive('myTabs', function(){
        return {
          restrict: 'E',
          transclude: true,
          scope: {},
          controller: ['$scope', function MyTabsCtrl($scope){
          var panes = $scope.panes = [];
          $scope.select = function(pane) {
                angular.forEach(panes, function(pane) {
                  pane.selected = false;
                });
                pane.selected = true;
            };
            this.addPane = function(pane){
              if (panes.length === 0){
                $scope.select(pane);
              }
              panes.push(pane);
            };
          }],
          templateUrl: 'view5/my-tabs.html'
        };
})
.directive('myPane', function(){
  return {
    require: '^^myTabs',
    restrict: 'E',
    transclude: true,
    scope: {
      title: '@'
    },
    link: function(scope, element, attrs, tabsCtrl){
      tabsCtrl.addPane(scope);
    },
    templateUrl: 'view5/my-pane.html'
  };
});
