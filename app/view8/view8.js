'use strict';

angular.module('myApp.view8', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view8', {
    templateUrl: 'view8/view8.html',
    controller: 'View8Ctrl as ctrl'
  });
}])
.controller('View8Ctrl', ['$scope', function($scope) {

  $scope.title = 'Components';
  $scope.lifeCycleHookTitle = 'Life cycle hook: $doCheck';
  this.hero =  {
    name: 'flash'
  };

}])
.controller('HeroDetailsCtrl', [ function(){
  var ctrl = this;
  ctrl.delete = function(){
    ctrl.onDelete({hero: ctrl.hero});
  };
  ctrl.update = function(prop, value) {
    ctrl.onUpdate({hero: ctrl.hero, prop: prop, value: value});
  };

}])
.controller('HeroListCtrl', ['$scope', '$element', '$attrs', function($scope, $element, $attrs){
  var ctrl = this;

  //load this via $http
  ctrl.list = [
    {
      name: 'Wonderwoman',
      location: 'Jupiter'
    },
    {
      name: 'Superman',
      location: 'Mars'
    },
    {
      name: 'Spiderman',
      location: 'Earth'
    },
    {
      name: 'Batman',
      location: 'Universe'
    }
  ];
  ctrl.updateHero = function(hero, prop, value){
    hero[prop] = value;
  };
  ctrl.deleteHero = function(hero) {
    var idx = ctrl.list.indexOf(hero);
    if (idx >= 0){
      ctrl.list.splice(idx, 1);
    }
  };
}])
.controller('EditableFieldCtrl', ['$scope', '$element', '$attrs', function($scope, $element, $attrs){

      var ctrl = this;
      ctrl.editMode = false;

      ctrl.handleModeChange = function() {
          if (ctrl.editMode){
            ctrl.onUpdate({ value: ctrl.fieldValue});
            ctrl.fieldValueCopy = ctrl.fieldValue;
          }
          ctrl.editMode = !ctrl.editMode;
      };

      ctrl.reset = function() {
        ctrl.fieldValue = ctrl.fieldValueCopy;
      };

      ctrl.$onInit = function() {
        //make copy of initial value to reset later
        ctrl.fieldValueCopy = ctrl.fieldValue;
        //set default field
        if (!ctrl.fieldType){
          ctrl.fieldType = 'text';
        }
      };
}])
.component('editableField', {
  templateUrl: 'view8/editable-field.html',
  controller: 'EditableFieldCtrl',
  bindings: {
    fieldValue: '<', //< input one way binding
    fieldType: '@?', //@ input one way binding for strings ? optional
    onUpdate: '&' //outputs use & function as callbacks to component events
  }
})
.component('heroDetails', {
  templateUrl: 'view8/hero-details.html',
  controller: 'HeroDetailsCtrl',
  bindings: {
    hero: '<',
    onDelete: '&',
    onUpdate: '&'
  }
})
.component('heroDetail', {
  templateUrl: 'view8/hero-detail.html',
  controller: 'View8Ctrl',
  bindings: {
    hero: '='
  }
})
.component('heroList', {
  templateUrl: 'view8/hero-list.html',
  controller: 'HeroListCtrl'
})
.component('app', {
  template:
    'Month: <input ng-model="$ctrl.month" ng-change="$ctrl.updateDate()">' +
    'Date: {{$ctrl.date}}' +
    '<test date="$ctrl.date"></test>',
    controller: function() {
      this.date = new Date();
      this.month = this.date.getMonth();
      this.updateDate = function() {
        this.date.setMonth(this.month);
      };
    }
})
.component('test', {
    bindings: { date: '<'},
    template:
      '<pre>{{ $ctrl.log | json }}</pre>',
    controller: function() {
      var previousValue;
      this.log = [];
      this.$doCheck = function() {
        var currentValue = this.date && this.date.valueOf();
        if (previousValue !== currentValue) {
          this.log.push('doCheck: date mutated: ' + this.date);
          previousValue = currentValue;
        }
      };
    }
});
