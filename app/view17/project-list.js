'use strict';

angular.module('myApp.view17').value('projectListValue', [
  {
    name: 'AngularJS',
    site: 'https://angularjs.org/',
    description: 'HTML enhanced for web apps!'
  },
  {
    name: 'Angular',
    site: 'https://angular.io/',
    description: 'One framework. Mobile and desktop.'
  },
  {
    name: 'jQuery',
    site: 'http://jquery.com/',
    description: 'Write less, do more. Supported with greed from redmond.'
  },
  {
    name: 'Backbone',
    site: 'http://backbonejs.org/',
    description: 'Models for your apps.'
  },
  {
    name: 'SproutCore',
    site: 'http://sproutcore.com/',
    description: 'A framework for innovative web apps.'
  },
  {
    name: 'Polymer',
    site: 'https://www.polymer-project.org/',
    description: 'Reusable components for the modern web.'
  },
  {
    name: 'Spine',
    site: 'http://spine.github.io/',
    description: 'Awesome MVC Apps.'
  },
  {
    name: 'Cappucino',
    site: 'http://www.cappuccino-project.org/',
    description: 'Objective-J.'
  },
  {
    name: 'KnockoutJS',
    site: 'http://knockoutjs.com/'
    description: 'MVVM pattern. Built with greed in redmond.'
  },
  {
    name: 'GWT',
    site: 'http://www.gwtproject.org/',
    description: 'JS in JAVA. Is Awesome!'
  },
  {
    name: 'Ember',
    site: 'http://emberjs.com/',
    description: 'Ambitious web apps.'
  },
  {
    name: 'React',
    site: 'https://facebook.github.io/react/',
    description: 'A JavaScript library for building user interfaces.'
  },
  {
    name: 'Handlebars',
    site: 'http://handlebarsjs.com/',
    description: 'Build semantic templates'
  }
])
