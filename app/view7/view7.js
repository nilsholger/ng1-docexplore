'use strict';

angular.module('myApp.view7', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view7', {
    templateUrl: 'view7/view7.html',
    controller: 'View7Ctrl as ctrl'
  });
}])

.controller('View7Ctrl', ['$scope', 'filterFilter', 'reverseFilter', 'decoration',
 function($scope, filterFilter, reverseFilter, decoration) {

  $scope.title = 'Filters';

  this.array = [
    {name: 'Tobias'},
    {name: 'Alex'},
    {name: 'Hans'},
    {name: 'Stephen'},
    {name: 'Rob'},
    {name: 'Carmen'},
    {name: 'Jeff'},
    {name: 'Brian'},
    {name: 'Igor'},
    {name: 'James'},
    {name: 'Brad'},
    {name: 'Nils'}
  ];
  this.filteredArray = filterFilter(this.array, 'a');

  $scope.greeting = 'hello';
  $scope.filteredGreeting = reverseFilter($scope.greeting);

  $scope.statefulFilterGreeting = 'hello carmen';
  $scope.decoration = decoration;
}])

.filter('reverse', function(){
  return function(input, uppercase){
    input = input || '';
    var out = '';
    for (var i = 0; i < input.length; i++){
      out = input.charAt(i) + out;
    }
    if (uppercase){
      out = out.toUpperCase();
    }
    return out;
  };
})

.filter('decorate', ['decoration', function(decoration){
        function decorateFilter(input){
          return decoration.symbol + input + decoration.symbol;
        }
        decorateFilter.$stateful = true;
        return decorateFilter;
}])
.value('decoration', {symbol: '*'});
