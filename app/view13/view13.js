'use strict';

angular.module('myApp.view13', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view13', {
    templateUrl: 'view13/view13.html',
    controller: 'View13Ctrl'
  });
}])
.controller('View13Ctrl', ['$scope', '$http', function($scope, $http) {
  $scope.title = 'HTTP';

  $scope.$watch('search', function() {
        fetch();
  });

  $scope.search = 'Million Dollar Baby';

  function fetch(){
    $http.get("https://www.omdbapi.com/?t=" + $scope.search + "&tomatoes=true&plot=full")
    .then(function(response) {
      $scope.details = response.data;
    });
    $http.get("https://www.omdbapi.com/?s=" + $scope.search).then(function(response){
      $scope.related = response.data;
    });
  }

}]);
