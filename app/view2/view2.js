'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$scope', function($scope) {

      var exprs = $scope.exprs = [];
      $scope.expr = '3 * 10 | currency';
      $scope.addExp = function(expr){
        exprs.push(expr);
      };

      $scope.removeExp = function(index){
        exprs.splice(index, 1);
      };

      $scope.name = 'World';
      $scope.greet = function(){
        window.alert('Hello ' + $scope.name);
      };

      $scope.clickMe = function(clickEvent){
        $scope.clickEvent = simpleKeys(clickEvent);
        console.log(clickEvent);
      };

      function simpleKeys(original){
        return Object.keys(original).reduce(function(obj, key){
          obj[key] = typeof original[key] === 'object' ? '{ ... }' : original[key];
          return obj;
        }, {});
      }

      var counter = 0;
      var names = ['Igor', 'Misko', 'Chirayu', 'Lucas', 'Nils'];

      $scope.clickMee = function(clickedEvent){
        $scope.firstName = names[counter % names.length];
        counter++;
      };



}]);
