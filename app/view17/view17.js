'use strict';

angular.module('myApp.view17', ['ngRoute', 'firebase'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view17', {
    templateUrl: 'view17/view17.html',
    controller: 'View17Ctrl'
  });
}])

.controller('View17Ctrl', ['$scope', '$firebaseObject', function($scope, $firebaseObject) {

  $scope.title = 'Firebase';

  var ref = firebase.database().ref().child("data");
  //download data in local Object
  var syncObject = $firebaseObject(ref);

  //synchronize object with three way databinding
  syncObject.$bindTo($scope, "data");

  $scope.projectList = [
    {
      name: 'AngularJS',
      site: 'https://angularjs.org/',
      description: 'HTML enhanced for web apps!'
    },
    {
      name: 'Angular',
      site: 'https://angular.io/',
      description: 'One framework. Mobile and desktop.'
    },
    {
      name: 'jQuery',
      site: 'http://jquery.com/',
      description: 'Write less, do more.'
    },
    {
      name: 'Backbone',
      site: 'http://backbonejs.org/',
      description: 'Models for your apps.'
    },
    {
      name: 'SproutCore',
      site: 'http://sproutcore.com/',
      description: 'A framework for innovative web apps.'
    },
    {
      name: 'Polymer',
      site: 'https://www.polymer-project.org/',
      description: 'Reusable components for the modern web.'
    },
    {
      name: 'Spine',
      site: 'http://spine.github.io/',
      description: 'Awesome MVC Apps.'
    },
    {
      name: 'Cappucino',
      site: 'http://www.cappuccino-project.org/',
      description: 'Objective-J.'
    },
    {
      name: 'KnockoutJS',
      site: 'http://knockoutjs.com/',
      description: 'MVVM pattern.'
    },
    {
      name: 'GWT',
      site: 'http://www.gwtproject.org/',
      description: 'JS in JAVA. Is Awesome!'
    },
    {
      name: 'Ember',
      site: 'http://emberjs.com/',
      description: 'Ambitious web apps.'
    },
    {
      name: 'React',
      site: 'https://facebook.github.io/react/',
      description: 'A JavaScript library for building user interfaces.'
    },
    {
      name: 'Handlebars',
      site: 'http://handlebarsjs.com/',
      description: 'Build semantic templates'
    }
  ];

}]);
