'use strict';

angular.module('myApp.view3', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view3', {
    templateUrl: 'view3/view3.html',
    controller: 'View3Ctrl as todoList'
  });
}])

.controller('View3Ctrl', ['$scope', function($scope) {
        $scope.title = 'Interpolation';
        $scope.username = 'wonderwoman';

        var todoList = this;

        todoList.todos = [
          { text: 'learn AngularJS', done: false},
          { text: 'learn Just Angular', done: false},
          { text: 'create Angular 1, Angular 2 Apps', done: false},
          { text: 'write new Angular Tutorials', done: false},
          { text: 'contribute in Angular Repositories on GitHub', done: false},
          { text: 'destroy windows && azure', done: false },
          { text: 'get up at 5:00 AM every Morning', done: true},
          { text: 'work hard, be kind, do more ...', done: false}
        ];

        todoList.addTodo = function() {
          todoList.todos.push({text: todoList.todoText, done: false});
          todoList.todoText = '';
        };

        todoList.remaining = function() {
          var count = 0;
          angular.forEach(todoList.todos, function(todo) {
            count += todo.done ? 0 : 1;
          });
          return count;
        };

        todoList.archive = function() {
          var oldTodos = todoList.todos;
          todoList.todos = [];
          angular.forEach(oldTodos, function(todo){
            if (!todo.done) { todoList.todos.push(todo); }
          });
        };
}]);
