'use strict';

angular.module('myApp.view4', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view4', {
    templateUrl: 'view4/view4.html',
    controller: 'View4Ctrl'
  });
}])
.controller('View4Ctrl', ['$scope', '$timeout', function($scope, $timeout) {

    $scope.technology = {
      name: 'Just Angular',
      url: 'https://angular.io'
    };

    $scope.muse = {
      name: 'Carmen',
      address: '10000 Mountainsummit'
    };

    $scope.hans = { name: 'Hans', address: 'San Francisco'};
    $scope.stephen = { name: 'Stephen', address: 'Silicon Valley'};
    $scope.rob = { name: 'Rob', address: 'Los Angeles'};

    $scope.format = 'dd/MM/yyyy hh:mm:ss a';

    $scope.compiler = 'Tobias';
    $scope.message = '';
    $scope.hideDialog = function(message){
      $scope.message = message;
      $scope.dialogIsHidden = true;
      $timeout(function(){
        $scope.message = '';
        $scope.dialogIsHidden = false;
      }, 2000);
    };
}])
.directive('myTechnology', function(){
  return {
    templateUrl: 'view4/my-technology.html'
  };
})
.directive('myMuse', function(){
  return {
    templateUrl: function(elem, attr){
          return 'view4/muse-' + attr.type + '.html';
    }
  };
})
.directive('myFriend', function(){
  return {
    restrict: 'E',
    scope: {
      friendInfo: '=info'
    },
    templateUrl: 'view4/my-friend-iso.html'
  };
})
.directive('myCurrentTime', ['$interval', 'dateFilter', function($interval, dateFilter){

    function link(scope, element, attrs){
        var format, timeoutId;

        function updateTime(){
          element.text(dateFilter(new Date(), format));
        }

        scope.$watch(attrs.myCurrentTime, function(value){
          format = value;
          updateTime();
        });

        element.on('$destroy', function(){
          $interval.cancel(timeoutId);
        });

        timeoutId = $interval(function(){
          updateTime();
        }, 1000);
    }
    return {
      link: link
    };
}])
.directive('myDialog', function(){
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    templateUrl: 'view4/my-dialog.html',
    link: function(scope) {
      scope.compiler = 'Jeff';
    }
  };
})
.directive('myDialogClose', function(){
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      'close': '&onClose'
    },
    templateUrl: 'view4/my-dialog-close.html'
  };
});
