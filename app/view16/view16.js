'use strict';

angular.module('myApp.view16', ['ngRoute', 'geolocation'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view16', {
    templateUrl: 'view16/view16.html',
    controller: 'View16Ctrl'
  });
}])

.controller('View16Ctrl', ['$scope', '$http', '$rootScope', 'geolocation',
function($scope, $http, $rootScope, geolocation) {

  $scope.title = 'Google Maps';

  $scope.formData = {};
   var coords = {};
   var lat = 0;
   var long = 0;

   $scope.formData.latitude = 52.4977581;
   $scope.formData.longitude = 13.4471495;

   $rootScope.$on("clicked", function() {

         $scope.$apply(function() {
           $scope.formData.latitude = parseFloat(gservice.clickLat).toFixed(3);
           $scope.formData.longitude = parseFloat(gservice.clickLong).toFixed(3);
           $scope.formData.htmlVerified = "no (thanks for stalking/spamming my map ...)";
         });
   });

}]);
