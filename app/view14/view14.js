'use strict';

angular.module('myApp.view14', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view14', {
    templateUrl: 'view14/view14.html',
    controller: 'View14Ctrl'
  })
  .when('/view14/:phoneId',{
    template: '<phone-detail></phone-detail>'
  })
  .otherwise('/view14')
}])
.filter('checkmark', function() {
  return function(input) {
        return input ? '\u2713' : '\u2718';
  };
})
.controller('View14Ctrl', ['$scope', function($scope) {
  $scope.title = 'Animations';
}])
.component('phoneList', {
  templateUrl: 'view14/phone-list.template.html',
  controller: ['Phone', function PhoneListCtrl(Phone) {
    this.phones = Phone.query();
    self.orderProp = 'age';
  }
]
})
.component('phoneDetail', {
  templateUrl: 'view14/phone-detail.template.html',
  controller: ['Phone', '$routeParams', function PhoneDetailCtrl(Phone, $routeParams){
              var self = this;

              self.setImage = function setImage(imageUrl) {
                self.mainImageUrl = imageUrl;
              };

              self.onDblClick = function onDblClick(imageUrl){
                alert('you double clicked: ' + imageUrl);
              };

              self.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone){
                    self.setImage(phone.images[0]);
              });
        }
  ]
});
