'use strict';

angular.module('myApp.view1', ['ngRoute', 'finance2'])

.config(['$routeProvider', function($routeProvider){
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl as invoice'
  });
}])
.controller('View1Ctrl', ['currencyConverter', function currencyConverter(currencyConverter) {
  this.qty = 1;
  this.cost = 2;
  this.inCurr = 'EUR';
  this.currencies = currencyConverter.currencies;
  this.usdToForeignRates = {
    USD: 1,
    EUR: 0.74,
    CNY: 6.09
  };

  this.total = function total(outCurr) {
    return currencyConverter.convert(this.qty * this.cost, this.inCurr, outCurr);
  };

  this.pay = function pay() {
    window.alert('Thanks!');
  };
}])
.controller('MovieCtrl', ['$scope', '$http', function($scope, $http) {
  $scope.title = 'HTTP';

  $scope.$watch('search', function() {
        fetch();
  });

  $scope.search = 'Suicide Squad';

  function fetch(){
    $http.get("https://www.omdbapi.com/?t=" + $scope.search + "&tomatoes=true&plot=full")
    .then(function(response) {
      $scope.details = response.data;
    });
    $http.get("https://www.omdbapi.com/?s=" + $scope.search).then(function(response){
      $scope.related = response.data;
    });
  }

}]);
