'use strict';

var INTEGER_REGEXP = /^-?\d+$/;

angular.module('myApp.view10', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view10', {
    templateUrl: 'view10/view10.html',
    controller: 'View10Ctrl'
  });
}])

.controller('View10Ctrl', ['$scope', function($scope) {

  $scope.title = 'More Forms';
  $scope.master = {};

  $scope.update = function(user) {
    $scope.master = angular.copy(user);
  };

  $scope.reset = function(form){
    if (form){
      form.$setPristine();
      form.$setUntouched();
    }
    $scope.user = angular.copy($scope.master);
  };

  $scope.reset();

}])

.directive('integer', function() {
      return {
              require: 'ngModel',
              link: function(scope, elm, attrs, ctrl) {
                    ctrl.$validators.integer = function(modelValue, viewValue) {
                          if (ctrl.$isEmpty(modelValue)){
                            //consider empty models to be valid
                            return true;
                          }
                          if (INTEGER_REGEXP.test(viewValue)){
                            //it is valid
                            return true;
                          }
                          //it is invalid
                          return false;
                    };
              }
      };
})
.directive('username', function($q, $timeout) {
        return {
          require: 'ngModel',
          link: function(scope, elm, attrs, ctrl) {
            var usernames = ['jim', 'john', 'jill', 'jack'];

            ctrl.$asyncValidators.username = function(modelValue, viewValue) {

                  if (ctrl.$isEmpty(modelValue)){
                    //consider empty model valid
                    return $q.resolve();
                  }

                  var def = $q.defer();

                  $timeout(function() {
                    //mock a delayed response
                    if (usernames.indexOf(modelValue) === -1) {
                      //username is available
                      def.resolve();
                    } else {
                      def.reject();
                    }
                  }, 1000);
                  return def.promise;
            };
          }
        };
})
.directive('overwriteEmail', function(){
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@gmail\.com$/i;
        return {
          require: '?ngModel',
          link: function(scope, elm, attrs, ctrl) {
            //only apply validator if ngModel present and ng has added the email validator
            if (ctrl && ctrl.$validators.email) {
            ctrl.$validators.email = function(modelValue) {
                    return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
            };
          }
          }
        };
})
.directive('contenteditable', function(){

        return {
          require: 'ngModel',
          link: function(scope, elm, attrs, ctrl){
              //view -> model
              elm.on('blur', function() {
                ctrl.$setViewValue(elm.html());
              });

              //model -> view
              ctrl.$render = function() {
                elm.html(ctrl.$viewValue);
              };

              //load init value from dom
              ctrl.$setViewValue(elm.html());
          }
        };
});
