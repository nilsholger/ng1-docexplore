'use strict';

angular.module('myApp.view6', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view6', {
    templateUrl: 'view6/view6.html',
    controller: 'View6Ctrl'
  });
}])

.controller('View6Ctrl', ['$scope', function($scope) {

  $scope.title = 'Views and Routes, see documentation';
  $scope.url = 'https://docs.angularjs.org/api/ngRoute/service/$route';


}]);
