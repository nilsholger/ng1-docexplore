'use strict';

angular.module('myApp.view9', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view9', {
    templateUrl: 'view9/view9.html',
    controller: 'View9Ctrl'
  });
}])

.controller('View9Ctrl', ['$scope', function($scope) {
    $scope.title = 'Simple Form';

    $scope.master = {};

    $scope.update = function(user){
      $scope.master = angular.copy(user);
    };

    $scope.reset = function() {
      $scope.user = angular.copy($scope.master);
    };

    $scope.reset();

    $scope.mastercf = {};
    $scope.usercf = {};

    $scope.updatecf = function(usercf) {
      $scope.mastercf = angular.copy(usercf);
    };

    $scope.resetcf = function() {
      $scope.usercf = angular.copy($scope.mastercf);
    };

    $scope.userdmu = {};

}]);
