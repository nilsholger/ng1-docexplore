'use strict';

angular.module('myApp.view12', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view12', {
    templateUrl: 'view12/view12.html',
    controller: 'View12Ctrl'
  });
}])

.controller('View12Ctrl', ['$scope', function($scope) {
  $scope.title = 'Scopes';
  $scope.username = 'World';
  $scope.sayHello = function() {
    $scope.greeting = 'Hello ' + $scope.username + '!';
  };
}])
.controller('GreetCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
        $scope.name = 'World';
        $rootScope.department = 'Just Angular';
}])
.controller('ListCtrl', ['$scope', function($scope) {
      $scope.names = ['Igor', 'Misko', 'Vojta', 'Nils'];
}])
.controller('EventCtrl', ['$scope', function($scope) {
      $scope.count = 0;
      $scope.$on('MyEvent', function() {
            $scope.count++;
      });
}]);
